<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;


class AdminBaseNotification extends Notification
{
    use Queueable;

    protected $fields;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['database', 'broadcast'];
    }

    public function toBroadcast($notifiable) {
        return (new BroadcastMessage($this->fields))
                    ->onConnection('redis')
                    ->onQueue('notifications.admin');
    }

    public function toDatabase($notifiable) {
        return $this->fields;
    }
}
