import axios from 'axios';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import { MessageBox } from '../../../MessageBox.js';

export const crudFunctionsMixin = {
	data: function() {
		return {
			dataTable: {
				loadingVal: true,
			},
			selected: [],
			collection: [],
			fetchModel: true
		}
	},
	computed: {
    	formInputVals: function() {
    		let obj = {};
    		for(let key of Object.keys(this.form.inputs)) {
    			obj[key] = this.form.inputs[key].val;
    		}
    		return obj;
    	},
    	tblShowFields: function() {
    		let obj = [];
    		for(let hdr of this.dataTable.headers) {
    			if(typeof hdr.value != "undefined") {
    				obj.push(hdr.value);
    			}
    		}
    		return obj;
    	}
    },
    watch: {
    	'$root.formMode': function(newMode) {
    		switch(newMode) {
    			case 'add':
    				this.form.titleText = `Add ${this.modelName}`;
            		this.clearForm();
            		break;

    			case 'edit':
    				this.form.titleText = `Save ${this.modelName}`;
            		break;
    		}
    	},
    	selected: function(newSelectedList) {
			this.$root.selected = newSelectedList;
		}
    },
    created() {
    	this.$root.modelName = this.modelName;
		this.$root.modelNamePlural = this.modelNamePlural;

		this.$root.toolbar 		= this.toolbar || {};
		this.$root.form 		= this.form || {};
		this.$root.formSearch 	= this.formSearch || {};

		if(this.fetchModel) {
			this.$root.fetch = this.fetch;
			this.$root.search = this.search;
			this.$root.save = this.save;
			this.$root.add = this.add;
			this.$root.deleteSelected = this.deleteSelected;

			this.getFetch();
    	}
	},
    methods: {
		fetch() {
			this.dataTable.loadingVal = true;

    		this.getFetch();
    	},
    	search() {
    		let params = {};
    		for(let key of Object.keys(this.formSearch.inputs)) {
    			params[key] = this.formSearch.inputs[key].val;
    		}

    		this.getFetch(params);
    	},
    	getFetch(params) {
    		axios.get(`/${this.model}/fetch`, { params: params })
				.then(r => {
					if(r.status == 200 || r.collection.length) {
	    				this.collection = r.data.collection.data || r.collection;

	    				this.dataTable.loadingVal = false;
	    			}
				});

    		this.$root.searchDialog = false;
    	},
    	getFromRedisCache(key, obj, index) {
    		if(this.$root.cache[key]) {
    			obj[index] = this.$root.cache[key];
    		} else {
        		axios.get('/cache', { params : { key: key } })
	    			.then(r => {
	    				if(r.status == 200) {
	    					obj[index] = r.data;
	    					this.$root.cache[key] = r.data;
	    				}
	    			});
    		}
    	},
    	add() {
    		let params = {};
    		for(let key of Object.keys(this.form.inputs)) {
    			if(this.form.inputs[key].add) {
    				params[key] = this.form.inputs[key].val;
    			}
    		}

    		axios.post(`/${this.model}/add`, params)
    			.then(r => {
    				if(r.data.errors) {
    					this.form.errors = r.data.errors;
    					this.$root.formDialog = true;
    				} else {
    					let newRow = this.formInputVals;

                		if(r.data.addFields) {
                			for(let key of Object.keys(r.data.addFields)) {
                				newRow[key] = r.data.addFields[key];
                			}
                		}
                		this.collection.push(Object.assign({}, newRow));

                		this.clearForm();
                		this.$root.formDialog = false;
    				}
    			}).
    			catch(e => {
    				if(e.response.data.errors) {
    					this.form.errors = e.response.data.errors;
    				}
    			});
    	},

    	edit(id) {
    		let edited = this.collection.filter(p => p.id == id)[0];

    		for(let key of Object.keys(edited)) {
    			if(this.form.inputs.hasOwnProperty(key)) {
    				this.form.inputs[key].val = edited[key];
    			}
    		}

    		this.$root.formDialog = true;
    		this.$root.formMode = "edit";
			window.scrollTo({ top: 0, behavior: 'smooth' });
		},

		save() {
			let params = {};
    		for(let key of Object.keys(this.form.inputs)) {
    			if(this.form.inputs[key].save) {
    				params[key] = this.form.inputs[key].val;
    			}
    		}

    		axios.put(`/${this.model}/edit`, params)
    			.then(r => {
    				if(r.data.errors) {
    					this.form.errors = r.data.errors;
    					this.$root.formDialog = true;
    				} else {
    					if(r.data.addFields) {
                			for(let key of Object.keys(r.data.addFields)) {
                				this.form.inputs[key].val = r.data.addFields[key];
                			}
                		}

                		Vue.set(this.collection,
                				this.collection.findIndex(row => row.id == this.formInputVals.id),
                				this.formInputVals);

                		this.$root.formDialog = false;
                		this.$root.formMode = "add";
    				}
    			}).

	    		catch(e => {
					if(e.response.data.errors) {
						this.form.errors = e.response.data.errors;
					}
	    		});
		},

		deleteRows(ids) {
			axios.delete(`/${this.model}`, { data: { ids: ids }} )
				.then(r => {
					if(!Array.isArray(ids)) {
						ids = [ids];
					}

					ids.forEach(id => {
	        			this.collection.splice(
	            			this.collection.indexOf(
	            				this.collection.filter(row => row.id == id)[0]
	            			), 1);
	        		});
				});
		},

		deleteSelected() {
    		MessageBox({
	            title: `<i class='fa fa-trash text-orange-dark'></i> Delete (${this.selected.length}) Selected ${this.modelNamePlural}`,
	            content: `Are you sure you want to delete the selected ${this.modelNamePlural}?`,
	            buttons: "[No][Yes]"
	        }, (e) => {
	        	if("Yes" == e) {
	        		this.deleteRows(this.selected.map(s => s.id));
	        	}
	        });
    	},

    	remove(id) {
			MessageBox({
	            title: `<i class='fa fa-trash text-orange-dark'></i> Delete ${this.modelName}`,
	            content: `Are you sure you want to delete this ${this.modelName}?`,
	            buttons: "[No][Yes]"
	        }, (e) => {
	        	if("Yes" == e) {
	        		this.deleteRows(id);
	        	}
	        });
		},

		clearForm() {
    		for(let key of Object.keys(this.formInputVals)) {
    			if(this.form.inputs.hasOwnProperty(key)) {
    				this.form.inputs[key].val = '';
    			}
    		}
    		this.form.errors = false;
    	},

		toggleAllSelected () {
			if (this.selected.length) this.selected = []
			else this.selected = this.collection.slice()
		},

		changeSort (column) {
			if (this.dataTable.pagination.sortBy === column) {
				this.dataTable.pagination.descending = !this.dataTable.pagination.descending;
			} else {
				this.dataTable.pagination.sortBy = column;
				this.dataTable.pagination.descending = false;
			}
		},

		toggleActive: function(id) {
			let s = this.collection.filter(row => row.id == id)[0];

			let newVal = (s.active) ? 0 : 1;

			axios.patch(`/${this.model}/update`, {
					id: 	s.id,
					active: newVal
				})
				.then(r => {
					if(r.status == 200) {
						s.active = newVal;
					}
				});
		}
	}
}