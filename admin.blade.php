<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<title>{{ config('app.name', 'OrderASlice 2.0') }}</title>
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,500,700">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">

    <link rel="stylesheet" media="screen, print" href="/css/app.css">
    <link rel="stylesheet" media="screen, print" href="/css/layouts.css">
    <link rel="stylesheet" media="screen, print" href="/css/form-validation.css">
	<link rel="stylesheet" media="screen, print" href="/css/MessageBox.css">

    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Material+Icons' rel="stylesheet" type="text/css">

    <script defer src="js/admin/admin-dashboard.js"></script>
    @stack('block-head')
</head>
<body>
    <div id="app" v-cloak>
		<v-app id="inspire">
			 <v-navigation-drawer fixed v-model="notifications.drawer" right :clipped="$vuetify.breakpoint.mdAndUp" app>
			 	<v-list-tile @click="notifications.drawer = false" dense>
                  	<v-list-tile-content>
                    	<v-subheader>Close Notifications</v-subheader>
                  	</v-list-tile-content>
                  	<v-list-tile-action><v-icon>close</v-icon></v-list-tile-action>
                </v-list-tile>

                <v-divider></v-divider>

         		<v-list v-if="notifications.msgs.length" three-line id="notificationsList">
     				<template v-for="(msg, i) in notifications.msgs" >
     					<v-slide-y-transition>
     						<template>
                    			<v-list-tile>
                    				<v-list-tile-avatar @click="markNotificationRead(msg.id)" title="Mark Notification as Read">
                    					<v-icon color="success">check_circle_outline</v-icon>
                    				</v-list-tile-avatar>

        							<v-tooltip bottom max-width="400" open-delay="1500">
                                    	<template v-slot:activator="{ on }">
                                      		<v-list-tile-content v-on="on" :to="msg.data.action_url">
                            					<v-list-tile-title class="green--text text--darken-3">@{{ msg.data.title }}</v-list-tile-title>
                            					<v-list-tile-sub-title class="caption" v-html="$options.filters.truncate(msg.data.content, 100)"></v-list-tile-sub-title>
                            				</v-list-tile-content>
                                    	</template>
                                    	<span v-html="msg.data.content"></span>
                                  	</v-tooltip>
                    			</v-list-tile>
                    		</template>
            			</v-fade-transition>
            		</template>
            	</v-list>
            </v-navigation-drawer>

            <v-navigation-drawer fixed :clipped="$vuetify.breakpoint.mdAndUp" app v-model="navMenu">
          		<v-list dense>
          			 <template v-for="item in navMenuItems">
                        <v-list-group v-if="item.subMenuItems" v-model="item.model" :key="item.text" :prepend-icon="item.icon"  :append-icon="item.model ? 'keyboard_arrow_up' : 'keyboard_arrow_down'">
                        	<v-list-tile slot="activator" :to="item.url">
                            	<v-list-tile-content>
                                	<v-list-tile-title>@{{ item.text }}</v-list-tile-title>
                              	</v-list-tile-content>
                            </v-list-tile>

                            <v-list-tile v-for="(child, i) in item.subMenuItems" :key="i" :to="child.url">
								<v-list-tile-content>
                                	<v-list-tile-title>@{{ child.text }}</v-list-tile-title>
                              	</v-list-tile-content>

                              	<v-list-tile-action v-if="child.icon">
                                	<v-icon>@{{ child.icon }}</v-icon>
                              	</v-list-tile-action>
                            </v-list-tile>
                        </v-list-group>

                        <v-list-tile v-else :key="item.text" :to="item.url">
                        	<v-list-tile-action>
                            	<v-icon>@{{ item.icon }}</v-icon>
                            </v-list-tile-action>

                            <v-list-tile-content>
                           		<v-list-tile-title>@{{ item.text }}</v-list-tile-title>
                            </v-list-tile-content>
                        </v-list-tile>
                	</template>
          		</v-list>
        	</v-navigation-drawer>

            <v-toolbar color="warning darken-1" dark app :clipped-left="true" fixed>
            	<v-toolbar-title style="width: 300px" class="ml-0 pl-3">
                	<v-toolbar-side-icon @click.stop="navMenu = !navMenu"></v-toolbar-side-icon>
                	<span class="hidden-sm-and-down">All Access To All Things</span>
              	</v-toolbar-title>

          		<v-spacer></v-spacer>

          		<v-btn icon>
                	<v-icon>apps</v-icon>
              	</v-btn>

              	<v-badge color="primary" overlap>
          			<template v-slot:badge>
            			<span v-if="notifications.msgs.length">@{{ notifications.msgs.length }}</span>
          			</template>
          			<v-icon :title="`(${notifications.msgs.length}) Notifications Pending Action`" large @click="notifications.drawer = !notifications.drawer">notifications</v-icon>
        		</v-badge>

				<v-btn  @click.stop="logout()" icon><v-icon>logout</v-icon></v-btn>
            </v-toolbar>

            <v-content>
            	<v-container fluid>
                	<v-layout justify-center row wrap>
                		<v-flex xs12>
                    		<v-toolbar dense absolute>
                              	<v-toolbar-title class="headline green--text text--darken-3"><i class="fas fa-address-card orange--text text--darken-2"></i> @{{ modelNamePlural }}</v-toolbar-title>
                              	<v-spacer></v-spacer>

								<v-toolbar-items  class="hidden-sm-and-down" v-for="(btn, index) in toolbar.btns" :key="index">
                                    <v-btn :disabled="btn.disabledIfNoneSelected && !selected.length" flat v-on="btn.events"><v-icon class="mr-1">@{{ btn.icon }}</v-icon> @{{ btn.text }}</v-btn>
                                </v-toolbar-items>

                              	<v-menu v-if="form.inputs || toolbar.menuItems" ref="toolbarMenu">
                              		<template v-slot:activator="{ on }">
                              			<v-toolbar-side-icon v-on="on" :title="modelNamePlural + ' Menu'"></v-toolbar-side-icon>
                                  	</template>

                            		<v-list>
                            			<v-list-tile @click.stop="searchDialog = !searchDialog;" v-if="formSearch.inputs">
                            				<v-list-tile-avatar><v-icon color="warning" class="mr-1">search</v-icon></v-list-tile-avatar>
                            				<v-list-tile-title>@{{ `Search ${modelNamePlural}` }}</v-list-tile-title>
                            			</v-list-tile>
                            			<v-list-tile @click.stop="fetch()" v-if="formSearch.inputs">
                            				<v-list-tile-avatar><v-icon color="error" class="mr-1">restore</v-icon></v-list-tile-avatar>
                            				<v-list-tile-title>@{{ `Reset ${modelNamePlural} Table` }}</v-list-tile-title>
                            			</v-list-tile>
                            			<v-divider></v-divider>
                            			<v-list-tile @click.stop="formDialog = !formDialog" v-if="form.inputs">
                            				<v-list-tile-avatar><v-icon color="success" class="mr-1">add_circle_outline</v-icon></v-list-tile-avatar>
                            				<v-list-tile-title>@{{ `Add New ${modelName}` }}</v-list-tile-title>
                            			</v-list-tile>
                            			<v-list-tile @click.stop="deleteSelected()" :disabled="!selected.length" v-if="form.inputs">
                            				<v-list-tile-avatar><v-icon color="error" class="mr-1">delete</v-icon></v-list-tile-avatar>
                            				<v-list-tile-title>@{{ `Delete Selected ${modelNamePlural}` }}</v-list-tile-title>
                            			</v-list-tile>

                            			<v-list-tile v-for="(item, index) in toolbar.menuItems" :key="index" v-on="item.events" :disabled="!selected.length">
                                    		<v-list-tile-avatar><v-icon :color="item.icon.color" class="mr-1">@{{ item.icon.icon }}</v-icon></v-list-tile-avatar>
                                    		<v-list-tile-title>@{{ item.title }}</v-list-tile-title>
                                    	</v-list-tile>
                            		</v-list>
                            	</v-menu>
                            </v-toolbar>
                        </v-flex>

                  		@yield('content')
                	</v-layout>
              	</v-container>
            </v-content>

            <v-dialog v-model="searchDialog" width="400px">
            	<v-card>
                	<v-card-title class="grey lighten-4 py-4 title">
                  		Search @{{ modelNamePlural }}
                	</v-card-title>
                	<v-container grid-list-sm class="pa-4">
                		<v-layout row wrap>
                			<v-flex xs12 v-for="(input, field) in formSearch.inputs">
                				<v-select
                					v-if="input.type == 'select'"
                						v-on="input.events"
                						v-show="!input.hide"
                    					:prepend-icon="input.icon"
                    					:placeholder="input.placeholder"
                    					v-model="input.val"
                    					:items="input.items"
                    					:hint="input.hint"
                    					:persistent-hint="input.hint ? true : false"></v-select>
                				<v-textarea
                					v-else-if="input.type == 'textarea'"
                						v-on="input.events"
                						v-show="!input.hide"
                    					:prepend-icon="input.icon"
                    					:placeholder="input.placeholder"
                    					v-model="input.val"></v-textarea>
                				<v-text-field
                					v-else
                						v-on="input.events"
                    					v-show="!input.hide"
                    					:prepend-icon="input.icon"
                    					:placeholder="input.placeholder"
                    					v-model="input.val"></v-text-field>
                			</v-flex>
                        </v-layout>
                	</v-container>
                    <v-card-actions>
                    	<v-spacer></v-spacer>
                    	<v-btn flat color="success" @click="fetch()"><span class="fa fa-ban mr-1"></span>Clear Search</v-btn>
                      	<v-btn flat color="error" @click="searchDialog = false"><span class="fa fa-ban mr-1"></span>Cancel</v-btn>
                      	<v-btn flat color="primary" @click="search()"><span class="fa fa-search mr-1"></span>Search</v-btn>
                    </v-card-actions>
            	</v-card>
        	</v-dialog>


            <v-dialog v-model="formDialog" width="800px">
            	<v-card>
                	<v-card-title class="grey lighten-4 py-4 title" v-html="form.titleText"></v-card-title>
                	<v-container grid-list-sm class="pa-4">
                  		<v-layout row wrap>
                			<v-flex xs12
                				v-for="(input, field) in form.inputs"
                    				v-if="input.gridSize && ((formMode == 'add' && input.add) || (formMode == 'edit' && input.save))"
                    				:xs12="input.gridSize == 12"
                    				:xs6="input.gridSize == 6"
                    				:xs4="input.gridSize == 4"
                    				:xs3="input.gridSize == 3"
                			>
                				<v-select
                					v-if="input.type == 'select'"
                					:prepend-icon="input.icon"
                					:placeholder="input.placeholder"
                					v-model="input.val"
                					:items="input.items"
                					:hint="input.hint"
                					:persistent-hint="input.hint ? true : false"
                					:error-messages="form.errors[field]">
                				</v-select>

                				<v-textarea
                					v-else-if="input.type == 'textarea'"
                					:prepend-icon="input.icon"
                					:placeholder="input.placeholder"
                					v-model="input.val"
                					:hint="input.hint"
                					:persistent-hint="input.hint ? true : false"
                					:error-messages="form.errors[field]">
                				</v-textarea>

                				<v-checkbox
                					v-else-if="input.type == 'checkbox'"
									v-bind:false-value="0"
									v-bind:true-value="1"
									:value="input.val"
                					:input-value="input.val"
                					v-model="input.val"
                					:error-messages="form.errors[field]"
                				>
                					<template #label>@{{ input.hint }}</template>
                				</v-checkbox>

                				<v-text-field
                					v-else
                					:prepend-icon="input.icon"
                					:placeholder="input.placeholder"
                					v-model="input.val"
                					:hint="input.hint"
                					:persistent-hint="input.hint ? true : false"
                					:auto-grow="input.autoGrow ? true : false"
                					:error-messages="form.errors[field]">
                				</v-text-field>
                			</v-flex>
                  		</v-layout>
                	</v-container>
                    <v-card-actions>
                      	<v-spacer></v-spacer>
                      	<v-btn flat color="error" @click="formDialog = false"><span class="fa fa-ban mr-1"></span>Cancel</v-btn>
                      	<v-btn flat color="primary" @click.prevent="(formMode == 'add') ? add() : save()"><v-icon>save</v-icon> <span v-html="form.titleText"></span></v-btn>
                    </v-card-actions>
          		</v-card>
        	</v-dialog>
      	</v-app>
    </div>

    <form id="logoutForm" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</body>
</html>