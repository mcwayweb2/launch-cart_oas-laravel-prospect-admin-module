<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Geocoder\OasGeocoder;
use App\Prospect;
use App\Http\Requests\Prospect\ValidateForm;
use App\Http\Requests\Prospect\ValidateRequestRestaurantForm;
use App\Events\Admin\ProspectAddedEvent;

class ProspectController extends Controller {
    protected $className = "Prospect";

    public function __construct() {
        parent::__construct();

        $this->middleware('auth')->except(['requestRestaurant', 'showRequestRestaurantForm', 'add']);
    }

    public function showRequestRestaurantForm() {
        return view('Prospect/request');
    }

    public function requestRestaurant(ValidateRequestRestaurantForm $request) {
        $validated = $request->validated();
        $validated['lead_source'] = 'Website';

        if(Auth::check() && ($request->input('notify') == "true")) {
            $validated['user_id'] = Auth::id();
        }

        unset($validated['notify']);
        unset($validated['g-recaptcha-response']);

        $prospect = Prospect::create($validated);

        $geocoder = new OasGeocoder(new \GuzzleHttp\Client());
        $coords = $geocoder->getCoordinatesForAddress($prospect->address.", ".$prospect->city." ".$prospect->zip);

        if(isset($coords['lat'])) {
            $prospect->latitude = $coords['lat'];
            $prospect->longitude = $coords['lng'];
            $prospect->save();
        }

        event(new ProspectAddedEvent($prospect));

        session()->flash('successMsg', 'Thanks. We will do our best to get your requested restaurant listed on our site.');
        return redirect('/restaurants/browse');
    }

    public function fetch(Request $request) {
        if($request->ajax()) {
            if($request->input('radius')) {
                //geoloc radius search
                $geocoder = new OasGeocoder(new \GuzzleHttp\Client());
                $coords = $geocoder->getCoordinatesForAddress($request->input('location'));

                if($coords['lat']) {
                    $prospects = Prospect::search($request->input('keywords'))
                                    ->aroundLatLng($coords['lat'], $coords['lng'])
                                    ->with([
                                        'hitsPerPage'   => 20,
                                        'aroundRadius'  => ($request->input('radius') * 1609)
                                    ]);
                }
            } else {
                $prospects = Prospect::search($request->input('keywords') ?: '');
            }

            if($request->input('contacted') !== null) {
                $prospects = $prospects->where('contacted', $request->input('contacted'));
            }

            $prospects = $prospects->paginate(1000);

            $prospects->load('foodcat');
            $prospects->load('state');

            return response()->json(['collection' => $prospects]);
        }
        return false;
    }

    public function add(ValidateForm $request) {
        if($request->ajax()) {
            $validated = $request->validated();

            $geocoder = new OasGeocoder(new \GuzzleHttp\Client());
            $coords = $geocoder->getCoordinatesForAddress($request->input('address').", ".$request->input('city')." ".$request->input('zip'));

            if(isset($coords['lat'])) {
                $validated['latitude'] = $coords['lat'];
                $validated['longitude'] = $coords['lng'];
            }

            $new_prospect = Prospect::create($validated);

            $new_prospect->user_id = Auth::id();
            $new_prospect->lead_source = "Manual";
            $new_prospect->save();

            event(new ProspectAddedEvent($new_prospect));

            return response()->json([
                'addFields'  => [
                    'id'        => $new_prospect->id,
                    'foodcat'   => \App\FoodCat::find($request->input('food_cat_id')),
                    'state'     => \App\State::find($request->input('state_id'))
                ]
            ]);
        }
        return false;
    }

    public function edit(ValidateForm $request) {
        if($request->ajax()) {
            $validated = $request->validated();

            Prospect::find($request->input('id'))->update($validated);

            return response()->json(['successMsg' => 'Lead Updated Sucessfully.']);
        }
        return false;
    }

}
