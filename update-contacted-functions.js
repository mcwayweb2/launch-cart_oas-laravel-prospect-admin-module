import axios from 'axios';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import moment from 'moment';

import { MessageBox } from '../../../MessageBox.js';

export const updateContactedFunctionsMixin = {
	methods: {
		updateContactedRows(ids) {
    		axios.patch(`/${this.model}/update`, {
	    			ids: ids,
	    			fields: {
	    				contacted: 				1,
	    				contacted_timestamp: 	moment().format('YYYY-MM-DD HH:mm:ss')
	    			}
	    		}).then(r => {
						if(!Array.isArray(ids)) {
							ids = [ids];
						}

						ids.forEach(id => {
							let updated = this.collection.filter(row => row.id == id)[0];
		            		updated.contacted = 1;
						});
					});
    	},

    	updateContactedSelected() {
			MessageBox({
				title: `<i class='fa fa-trash text-orange-dark'></i> Mark (${this.selected.length}) Selected ${this.modelName} as Contacted`,
	            content: `Are you sure you want to mark the selected ${this.modelName} as contacted?`,
	            buttons: "[No][Yes]"
	        }, (e) => {
	        	if("Yes" == e) {
	        		this.updateContactedRows(this.selected.map(s => s.id));
	        		this.selected.length = 0;
	        	}
	        });
		},

    	updateContacted(id) {
			MessageBox({
	            title: `<i class='fa fa-trash text-orange-dark'></i> Mark ${this.modelName} as Contacted?`,
	            buttons: "[No][Yes]"
	        }, (e) => {
	        	if("Yes" == e) {
	        		this.updateContactedRows(id);
	        	}
	        });
    	}
	}
}